import java.util.List;

public class ASTLet implements ASTNode {
	
	public ASTNode body;
	public List<String> ids;
	public List<ASTNode> vals;
	
	public ASTLet(List<String> ids, List<ASTNode> vals, ASTNode body) {
		this.ids = ids;
		this.vals = vals;
		this.body = body;
	}
	
	@Override
	public int eval(EnvI env) throws Exception {
		
		env=env.beginScope();
		
		
		for(int i=0; i<ids.size(); i++) {
			int value = vals.get(i).eval(env);
			env.assoc(ids.get(i), value);
		}
		
		
		int res = body.eval(env);
		
		env.endScope();
		
		
		return res;
		
		
	}

	@Override
	public void compile(EnvC env, CodeGenerator c) throws Exception {
		c.createFrame(ids.size());
		
		env.beginScope(c);
		
		
		
		for(int i=0; i<ids.size(); i++) {
			c.writeALoadSL();
			vals.get(i).compile(env,c);
			env.assoc(ids.get(i),c);
		}
		
		body.compile(env, c);
		
		env.endScope(c);
		
		
		
		
	}

}
