import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CodeGenerator {

	private static final String FRAME_NAME="fr";
	private static final String VALUE_NAME="x";
	private static final List<String> INIT = Arrays.asList(".method public <init>()V","aload_0",
			"invokenonvirtual java/lang/Object/<init>()V", "return",".end method");
	private static final String TEMPLATE="./Main.j";
	int counterFrame=-1;
	List<String> instructions;
	
	
	public CodeGenerator() {
		instructions=new ArrayList<String>();
	}
	
	public void write(String instruction) {
		instructions.add(instruction);
	}
	
	public void createFrame(int variables) throws IOException {
		List <String> lines= new ArrayList<String>();
		counterFrame++;
		Path file= Paths.get(FRAME_NAME+counterFrame+".j");
		lines.add(".class "+FRAME_NAME+counterFrame);
		lines.add(".super java/lang/Object");
		if(counterFrame==0)
			lines.add(".field public sl Ljava/lang/Object;");
		else lines.add(".field public sl L"+FRAME_NAME+(counterFrame-1)+";");
		for (int i=0;i<variables;i++) {
			lines.add(".field public "+VALUE_NAME+i+" I");
		}
		lines.add("\n");
		lines.addAll(INIT);
		
		
		Files.write(file,lines,StandardCharsets.UTF_8);
		
	}
	
	public void writeALoadSL() {
		instructions.add("aload 4");
	}
	
	public void writeDup() {
		instructions.add("dup");
	}
	
	public void writeAStoreSL() {
		instructions.add("astore 4");
	}
	
	public void writePutField(LocationVar p) {
		instructions.add("putfield "+FRAME_NAME+counterFrame+"/"+VALUE_NAME+p.getOffset()+" I");
	}
	
	public void writeGetField(LocationVar p) {
		int currentFrame=counterFrame;
		for(int i=0;i<p.getLevel();i++) {
			writeALoadSL();
			instructions.add("getfield "+FRAME_NAME+currentFrame+"/sl L"+FRAME_NAME+(currentFrame-1)+";");
			currentFrame--;
		}
		instructions.add("getfield "+FRAME_NAME+currentFrame+"/"+VALUE_NAME+p.getOffset()+" I");
	}
	
	public void writeBeginScope(boolean firstFrame) {
		write("new "+FRAME_NAME+counterFrame);
		writeDup();
		write("invokespecial "+FRAME_NAME+counterFrame+"/<init>()V");
		writeDup();
		writeALoadSL();
		if(firstFrame)
			write("putfield "+FRAME_NAME+counterFrame+"/sl Ljava/lang/Object;");
		else write("putfield "+FRAME_NAME+counterFrame+"/sl L"+FRAME_NAME+(counterFrame-1)+";");
		writeAStoreSL();
		
	};
	
	public void writeEndScope(boolean lastFrame) {
		writeALoadSL();
		if(lastFrame)
			write("getfield "+FRAME_NAME+counterFrame+"/sl Ljava/lang/Object;");
		else write("getfield "+FRAME_NAME+(counterFrame+1)+"/sl L"+FRAME_NAME+counterFrame+";");
		writeAStoreSL();
	}
	
	
	
	public void emit(String filename) throws IOException {
		Path template=Paths.get(TEMPLATE);
		Path file=Paths.get(filename+".j");
		
		List<String> lines = Files.readAllLines(template,StandardCharsets.UTF_8);
		int position=29;
		for (String line:instructions) {
			lines.add(position++,line);
		}
		Files.write(file, lines, StandardCharsets.UTF_8);
	}
	
	

	
	
	

	
	
	
	
	
}
