import java.util.Dictionary;
import java.util.Hashtable;

public class EnvI {

	EnvI ancestor;
	Dictionary<String,Integer> variables;
	
	public EnvI(EnvI env) {
		ancestor = env;
		variables = new Hashtable<String,Integer>();
	}
	
	int find(String id) throws Exception {
		Integer valueId= variables.get(id);
		
		if(valueId==null) {
			if(ancestor==null) {
				throw new Exception("Id not declered");
			}
			return ancestor.find(id);
		}
		else return valueId.intValue();
	}
	
	void assoc(String id,int value) throws Exception {
		
		if(variables.get(id)!=null) {
			throw new Exception("Id declared twice");
		}
		else variables.put(id, value);
	}
	
	EnvI beginScope() {
		return new EnvI(this);
	}
	
	EnvI endScope() {
		return ancestor;
	}
	
	
}
