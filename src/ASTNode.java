

public interface ASTNode {

	int eval(EnvI v) throws Exception;
	
	void compile(EnvC v, CodeGenerator c) throws Exception;
}
