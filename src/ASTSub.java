
public class ASTSub implements ASTNode {

	ASTNode lhs;
	ASTNode rhs;
	
	public ASTSub(ASTNode l, ASTNode r) {
		lhs = l;
		rhs = r;
	}

	@Override
	public int eval(EnvI env) throws Exception {
		int temp1 = lhs.eval(env);
		int temp2 = rhs.eval(env);
		return temp1-temp2;
	}

	@Override
	public void compile(EnvC env, CodeGenerator c) throws Exception {
		lhs.compile(env,c);
		rhs.compile(env,c);
		c.write("isub");
	}
	
	
	
}
