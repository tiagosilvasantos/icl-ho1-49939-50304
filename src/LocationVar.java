
public class LocationVar {

	private int level;
	private int offset;
	
	public LocationVar(int level,int offset) {
		this.level=level;
		this.offset=offset;
	}
	
	public int getLevel() {
		return level;
	}
	
	public int getOffset(){
		return offset;
	}
}
