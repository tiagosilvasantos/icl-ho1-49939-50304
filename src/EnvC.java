import java.util.ArrayList;
import java.util.List;



public class EnvC {
	int currentFrame;
	List<ArrayList<String>> variables;
	
	
	
	public EnvC() {
		currentFrame=-1;
		variables=new ArrayList<ArrayList<String>>();
	}
	
	public void assoc(String id,CodeGenerator c) throws Exception {
		for(String s:variables.get(currentFrame)) {
			if (s.equals(id))
				throw new Exception("Id declared twice");
		}
		variables.get(currentFrame).add(id);
		
		c.writePutField(new LocationVar(0, variables.get(currentFrame).size()-1));
	}
	
	public void find(String id,CodeGenerator c) throws Exception {
		boolean find=false;
		int level=0;
		int offset = 0;
		for (int i=currentFrame;i>=0;i--) {
			c.writeALoadSL();
			find=false;
			while(!find) {
				offset=0;
				for(String s:variables.get(i)) {
					if (s.equals(id)) {
						c.writeGetField(new LocationVar(level, offset));
						return;
					}
					offset++;	
				}
			}
			level++;
		}
		if(!find)
			throw new Exception("Id not declered");
	}
	
	public void beginScope(CodeGenerator c) {
		currentFrame++;
		variables.add(currentFrame, new ArrayList<String>());
		if(currentFrame==0)
			c.writeBeginScope(true);
		else c.writeBeginScope(false);
	}
	
	public void endScope(CodeGenerator c) {
		currentFrame--;
		if(currentFrame==-1)
			c.writeEndScope(true);
		else c.writeEndScope(false);
	}
	
	
	
	
}
