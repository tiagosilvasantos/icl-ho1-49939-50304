
public class ASTNum implements ASTNode {

	int val;
	
	public ASTNum(int v) {
		val = v;
	}
	
	@Override
	public int eval(EnvI env) {
		return val;
	}

	@Override
	public void compile(EnvC v, CodeGenerator c) {
		String inst = "sipush "+Integer.toString(val);
		c.write(inst);
		
	}

}
