
public class ASTUnaryMinus implements ASTNode {

	private ASTNode exp;
	
	
	public ASTUnaryMinus(ASTNode exp) {
		this.exp = exp;
	}
	
	@Override
	public int eval(EnvI env) throws Exception {
		return -exp.eval(env);
	}

	@Override
	public void compile(EnvC env,CodeGenerator c) throws Exception {
		exp.compile(env,c);
		c.write("sipush -1");
		c.write("imul");		
	}
	
	

}
