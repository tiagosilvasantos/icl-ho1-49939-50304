
public class ASTId implements ASTNode {

	String id;
	
	public ASTId(String id) {
		this.id = id;
	}
	
	@Override
	public int eval(EnvI env) throws Exception {
		return env.find(id);
	}

	@Override
	public void compile(EnvC env,CodeGenerator c) throws Exception {
		env.find(id,c);
		
	}

}
